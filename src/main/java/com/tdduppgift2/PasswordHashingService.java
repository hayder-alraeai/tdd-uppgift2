package com.tdduppgift2;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Base64;

public class PasswordHashingService {
    private final int ITERATIONS = 65536;
    private final int KEY_LENGTH = 512;
    private final String SECRET_KEY = "someSecretKey";
    private final String ALGORITHM = "PBKDF2WithHmacSHA512";


    public boolean verifyPassword(String password, String hashedPassword) {
        String optEncrypted = hashPassword(password);
        if (optEncrypted.isEmpty()) return false;
        return optEncrypted.equals(hashedPassword);
    }

    public String hashPassword(String actualPass) {
        char[] chars = actualPass.toCharArray();
        byte[] bytes = SECRET_KEY.getBytes();
        String hashedPassword = null;
        PBEKeySpec spec = new PBEKeySpec(chars, bytes, ITERATIONS, KEY_LENGTH);

        Arrays.fill(chars, Character.MIN_VALUE);

        try {
            SecretKeyFactory fac = SecretKeyFactory.getInstance(ALGORITHM);
            byte[] securePassword = fac.generateSecret(spec).getEncoded();
            hashedPassword =  Base64.getEncoder().encodeToString(securePassword);

        } catch (NoSuchAlgorithmException ex) {
            try {
                throw new NoSuchAlgorithmException();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

        }catch (InvalidKeySpecException ex){
            try {
                throw new InvalidKeySpecException();
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            }
        } finally{
            spec.clearPassword();
        }
        return hashedPassword;
    }
}
