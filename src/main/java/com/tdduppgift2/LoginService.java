package com.tdduppgift2;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.util.Arrays.asList;

public class LoginService {
    private PasswordHashingService passwordHashingService;
    private List<User> users = new ArrayList();
    public LoginService(PasswordHashingService passwordHashingService) {
        this.passwordHashingService = passwordHashingService;
        users.add(new User("anna", //not hashed password is "losen"
                "9UyrYbw6HOvHneTiJUowrVJh4DU1aOgCt+aqbKlSEqfP03P07klZ+Iycuqgt8lgIG8hZZuroRjqgaM2vNqEkLg==",
                asList("READ")));
        users.add(new User("berit",//not hashed password is "123456"
                "phQwjddiDcPGwh+kGsYN0Cj+8kxk+YT3gNQUcerKPva+5dKUJ8mfcqJACL729r7nx/qQyKGiUSz4wDpYJSb2Ow==",
                asList("READ", "WRITE")));
        users.add(new User("kalle", //not hashed password is "password"
                "password",
                asList("LCmoyNpgFn4YivOhe7ikY+OV1TA3bvPGg1/LrjT8r72XSefg7gZOTNiQqeKa8rTSNBnLVrTqTZAupR2I1KXWiw==")));
    }

    public String login(String username, String password) throws Exception {
        if (users.stream().anyMatch(user -> user.getUsername().equals(username) &&
                passwordHashingService.verifyPassword(password, user.getPassword())))
            return generateToken(username, users.stream().filter(u -> u.getUsername().equals(username)).findFirst().get().getAuthorities());
        throw new Exception("Wrong username or password");
    }

    public boolean verifyToken(String token) {
        String username = Jwts.parser()
                .setSigningKey("someSecretText")
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        if (users.stream().anyMatch(user -> user.getUsername().equals(username)))
            return true;
        return false;
    }

    public String generateToken(String username, List<String> authorities) {
        return Jwts.builder()
                .signWith(SignatureAlgorithm.HS512, "someSecretText")
                .setExpiration(new Date(System.currentTimeMillis() * 10))
                .claim("Authorities", authorities)
                .setSubject(username)
                .compact();
    }

    public List<String> getAuthorities(String token, String resource) throws Exception {
        Jws<Claims> jws = Jwts.parser()
                .setSigningKey("someSecretText")
                .parseClaimsJws(token);
        List<String> role = (List<String>) jws.getBody().get("Authorities");
        if (resource.equalsIgnoreCase("ACCOUNT")){
            if (role.contains("READ"))
                return role;
            else if (role.contains("WRITE"))
                return role;
        }else if (resource.equalsIgnoreCase("PROVISION_CALC")){
            if (role.contains("EXECUTE"))
                return role;
        }
        throw new Exception("NOT AUTHORIZED");
    }
}
