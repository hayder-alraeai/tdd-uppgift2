package com.tdduppgift2;

import jdk.jfr.Description;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PasswordHashingServiceTest {
    private PasswordHashingService passwordHashingService;
    @BeforeEach
    void setUp() {
        passwordHashingService = new PasswordHashingService();
    }

    @Test
    @Description(value = "should fail when comparing actual pass with hashed pass")
    void generate_hashed_password_test() {
        String actualPass = "123";
        String hashedPass = passwordHashingService.hashPassword(actualPass);
        System.out.println(passwordHashingService.hashPassword("password"));
        assertFalse(actualPass.equals(hashedPass));
    }

    @Test
    @Description(value = "should return true")
    void verify_passwords_test() {
        String hashedPass = passwordHashingService.hashPassword("123");
        boolean isPasswordValid = passwordHashingService.verifyPassword("123", hashedPass);
        assertTrue(isPasswordValid);
    }

    @Test
    @Description(value = "should return false when sending wrong credentials")
    void verify_password_with_wrong_credentials_test() {
        boolean isPasswordValid = passwordHashingService.verifyPassword("123", "wrongPass");
        assertFalse(isPasswordValid);
    }
}